if [[ -f "$HOME/.cargo/env" ]]; then
  . "$HOME/.cargo/env"
fi

# exports all defined variables
set -a

AGNOSTER_PROMPT_SEGENTS=(
	prompt_context
	prompt_dir
	prompt_virtualenv
	prompt_git
	prompt_status
	prompt_end
)

EDITOR=nvim
VISUAL=nvim
KUBE_EDITOR='nvim'
GOPATH='/home/deux/.local/share/go'
PATH=$PATH:/var/lib/snapd/snap/bin

LANGUAGE=fr_FR
LC_TIME=fr_FR.UTF-8
LC_CTYPE=en_US.UTF-8
LC_ALL=en_US.UTF-8

# disable automatic export of all variables
set +a
