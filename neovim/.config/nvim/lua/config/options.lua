-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

-- Disable format on save
vim.g.autoformat = false

-- Wrap lines
vim.opt.wrap= true

vim.g.copilot_auth_provider_url = "https://ag2r.ghe.com"
