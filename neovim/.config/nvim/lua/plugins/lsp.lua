return {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        "ansible-lint",
	"hadolint",
	"java-debug-adapter",
	"java-test",
      }
    }
  }
}
