# Dotfiles

## Prerequisites

* stow : allows to create automatic links

## Installation

Run the following command :

```shell
git clone git@gitlab.com:edeux/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
stow *
```

